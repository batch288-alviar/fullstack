let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    collection[collection.length] = element;

    return collection
}

function dequeue() {
    // In here you are going to remove the first element in the array

  for (let index = 0; index < collection.length - 1; index++) {
    collection[index] = collection[index + 1];
  }

  collection.length--;

  return collection;
}

function front() {
    // retrieve first element

    return collection[0];
}

function size() {
     // Number of elements   

    return collection.length
}

function isEmpty() {
    //it will check whether the function is empty or not

    return collection.length === 0;

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};