import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link, useNavigate, Navigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

export default function Register(){
	const [firstName, setFirst] = useState('');
	const [lastName, setLast] = useState('');
	const [mobile, setMobile] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//we consume the setUser function from the UserContext
	const {user, setUser} = useContext(UserContext);


	//we containe the useNavigate to navigate variable;
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	//we have to use the useEffect in enabling the submit button
	useEffect(()=>{
		if (firstName !== '' && lastName !== '' && mobile !== '' && mobile.length >= 11 && email !== '' && password1 !=='' && password2 !== '' && password1 === password2 && password1.length > 6){

			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}


	}, [firstName, lastName, mobile, email, password1, password2]);


	function register(event){
		event.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobile,
				email: email,
				password: password1
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title:'Duplicate user found',
					icon: 'error',
					text: 'Please use a different email or try logging in.'
				})
			}else{
				Swal2.fire({
					title: 'Success',
					icon: 'success',
					text: 'You are now registered!'
				})
				navigate('/login');
			}
		});


	// function that will be triggered once we submit the form
	// function register(event){
	// 	it prevents our pages to reload when submitting the forms
	// 	event.preventDefault()

	// 	localStorage.setItem('email', email);
	// 	setUser(localStorage.getItem('email'));

	// 	alert('Thank you for registering!');


		

		//clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
		// navigate('/')
	}

return(
	user.id === null 
	?
	<Container className = 'mt-5'>
		<Row>
			<Col className = 'col-6 mx-auto'>
				<h1 className = 'text-center'>Register</h1>
					<Form onSubmit={register}>
						<Form.Group className="mb-3" controlId="formFirstName">
						  <Form.Label>First Name</Form.Label>
						  <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange = {event => setFirst(event.target.value)}/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formLastName">
						  <Form.Label>Last Name</Form.Label>
						  <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange = {event =>	setLast(event.target.value)}/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formMobile">
						  <Form.Label>Mobile No.</Form.Label>
						  <Form.Control type="text" placeholder="Enter Mobile Number" value={mobile} onChange = {event => setMobile(event.target.value)}/>
						</Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control type="email" placeholder="Enter email" value={email} onChange = {event =>	setEmail(event.target.value)}/>
					      </Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control type="password" placeholder="Password" value = {password1} onChange = {event => setPassword1(event.target.value)}/>
					      </Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Confirm Password</Form.Label>
					        <Form.Control type="password" placeholder="Retype your password" value = {password2} onChange = {event => setPassword2(event.target.value)} />
					      </Form.Group>
					      <p>Have an account already? <Link to = '/login'>Log in here</Link>.</p>
					      <Button variant="primary" type="submit" disabled = {isDisabled}>
					        Submit
					      </Button>
					    </Form>

			</Col>
		</Row>
	</Container>
	
	:

	<Navigate to = '/notAccessible'/>

	)
}