import {Container, Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function PageNotFound(){
	return (
		<>
			<Container>
				<Row>
					<Col className = "mt-5">
						<h2>Zuitt Booking</h2>
						<h1>Page Not Found</h1>
						<p>Go back to the <a href='/'>homepage</a></p>
					</Col>
				</Row>
			</Container>
		</>
		)
}
