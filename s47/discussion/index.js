// console.log("hello")

// Section: Document Object Model (DOM)
	//  ALlow us to access or modify the properties of an html element in a webpage
	// It is standard on how to get, change, add or delete HTML elements
	// We will be focusing only with DOM in terms of managing forms

	// using the querySelector it can access the HTML elements
		// CSS selectors to target specific element
			// id selector(#);
			// class selector(.);
			// tag/type selector(html tags);
			// attribute selector([attribute]);

	// Query selectors have two types : querySelector and querySelectorAll
		// let firstElement = document.querySelector("#txt-first-name");
		// console.log(firstElement);


	// querySelector
	let secondElement = document.querySelector(".full-name");
	console.log(secondElement);
	// querySelectorAll
	let thirdElement = document.querySelectorAll(".full-name");
	console.log(thirdElement);

	// getElements
	let element = document.getElementById("fullName");
	console.log(element);

	element = document.getElementsByClassName("full-name");
	console.log(element);

// Section: Event Listeners
	// whenever a user interacts with a webpage, this action is considered as event
	// working with events is a large part of creating interactivity in a webpage
	// specific functions will be invoked if the event happens

	// Function "addEventListener", it takes two arguments
		// first argument is a string identifying the event
		// second argument is a function that the listener will invoke once the specified event occurs.

	let fullName = document.querySelector("#fullName");


	let firstElement = document.querySelector("#txt-first-name");

	let txtLastName = document.querySelector("#txt-last-name");

	firstElement.addEventListener("keyup", () => {
		console.log(firstElement.value);

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	})

	txtLastName.addEventListener("keyup", () => {
		console.log(txtLastName.value);

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	})

  let textColor = document.getElementById('text-color');

  textColor.addEventListener('change', () => {

    fullName.style.color = textColor.value;
  });
